/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.78
        Device            :  PIC18F45K22
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.05 and above
        MPLAB 	          :  MPLAB X 5.20	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set ESTOP_SW aliases
#define ESTOP_SW_TRIS                 TRISAbits.TRISA0
#define ESTOP_SW_LAT                  LATAbits.LATA0
#define ESTOP_SW_PORT                 PORTAbits.RA0
#define ESTOP_SW_ANS                  ANSELAbits.ANSA0
#define ESTOP_SW_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define ESTOP_SW_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define ESTOP_SW_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define ESTOP_SW_GetValue()           PORTAbits.RA0
#define ESTOP_SW_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define ESTOP_SW_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define ESTOP_SW_SetAnalogMode()      do { ANSELAbits.ANSA0 = 1; } while(0)
#define ESTOP_SW_SetDigitalMode()     do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set RESET_SW aliases
#define RESET_SW_TRIS                 TRISAbits.TRISA1
#define RESET_SW_LAT                  LATAbits.LATA1
#define RESET_SW_PORT                 PORTAbits.RA1
#define RESET_SW_ANS                  ANSELAbits.ANSA1
#define RESET_SW_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define RESET_SW_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define RESET_SW_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define RESET_SW_GetValue()           PORTAbits.RA1
#define RESET_SW_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define RESET_SW_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define RESET_SW_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define RESET_SW_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set OILP_SW aliases
#define OILP_SW_TRIS                 TRISAbits.TRISA2
#define OILP_SW_LAT                  LATAbits.LATA2
#define OILP_SW_PORT                 PORTAbits.RA2
#define OILP_SW_ANS                  ANSELAbits.ANSA2
#define OILP_SW_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define OILP_SW_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define OILP_SW_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define OILP_SW_GetValue()           PORTAbits.RA2
#define OILP_SW_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define OILP_SW_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define OILP_SW_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define OILP_SW_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set FINALP_SW aliases
#define FINALP_SW_TRIS                 TRISAbits.TRISA3
#define FINALP_SW_LAT                  LATAbits.LATA3
#define FINALP_SW_PORT                 PORTAbits.RA3
#define FINALP_SW_ANS                  ANSELAbits.ANSA3
#define FINALP_SW_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define FINALP_SW_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define FINALP_SW_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define FINALP_SW_GetValue()           PORTAbits.RA3
#define FINALP_SW_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define FINALP_SW_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define FINALP_SW_SetAnalogMode()      do { ANSELAbits.ANSA3 = 1; } while(0)
#define FINALP_SW_SetDigitalMode()     do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set OFF_SW aliases
#define OFF_SW_TRIS                 TRISAbits.TRISA5
#define OFF_SW_LAT                  LATAbits.LATA5
#define OFF_SW_PORT                 PORTAbits.RA5
#define OFF_SW_ANS                  ANSELAbits.ANSA5
#define OFF_SW_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define OFF_SW_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define OFF_SW_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define OFF_SW_GetValue()           PORTAbits.RA5
#define OFF_SW_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define OFF_SW_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define OFF_SW_SetAnalogMode()      do { ANSELAbits.ANSA5 = 1; } while(0)
#define OFF_SW_SetDigitalMode()     do { ANSELAbits.ANSA5 = 0; } while(0)

// get/set ON_SW aliases
#define ON_SW_TRIS                 TRISBbits.TRISB0
#define ON_SW_LAT                  LATBbits.LATB0
#define ON_SW_PORT                 PORTBbits.RB0
#define ON_SW_WPU                  WPUBbits.WPUB0
#define ON_SW_ANS                  ANSELBbits.ANSB0
#define ON_SW_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define ON_SW_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define ON_SW_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define ON_SW_GetValue()           PORTBbits.RB0
#define ON_SW_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define ON_SW_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define ON_SW_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define ON_SW_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define ON_SW_SetAnalogMode()      do { ANSELBbits.ANSB0 = 1; } while(0)
#define ON_SW_SetDigitalMode()     do { ANSELBbits.ANSB0 = 0; } while(0)

// get/set MOTOR_ON_1 aliases
#define MOTOR_ON_1_TRIS                 TRISBbits.TRISB1
#define MOTOR_ON_1_LAT                  LATBbits.LATB1
#define MOTOR_ON_1_PORT                 PORTBbits.RB1
#define MOTOR_ON_1_WPU                  WPUBbits.WPUB1
#define MOTOR_ON_1_ANS                  ANSELBbits.ANSB1
#define MOTOR_ON_1_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define MOTOR_ON_1_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define MOTOR_ON_1_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define MOTOR_ON_1_GetValue()           PORTBbits.RB1
#define MOTOR_ON_1_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define MOTOR_ON_1_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define MOTOR_ON_1_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define MOTOR_ON_1_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define MOTOR_ON_1_SetAnalogMode()      do { ANSELBbits.ANSB1 = 1; } while(0)
#define MOTOR_ON_1_SetDigitalMode()     do { ANSELBbits.ANSB1 = 0; } while(0)

// get/set MOTOR_ON_2 aliases
#define MOTOR_ON_2_TRIS                 TRISBbits.TRISB2
#define MOTOR_ON_2_LAT                  LATBbits.LATB2
#define MOTOR_ON_2_PORT                 PORTBbits.RB2
#define MOTOR_ON_2_WPU                  WPUBbits.WPUB2
#define MOTOR_ON_2_ANS                  ANSELBbits.ANSB2
#define MOTOR_ON_2_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define MOTOR_ON_2_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define MOTOR_ON_2_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define MOTOR_ON_2_GetValue()           PORTBbits.RB2
#define MOTOR_ON_2_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define MOTOR_ON_2_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define MOTOR_ON_2_SetPullup()          do { WPUBbits.WPUB2 = 1; } while(0)
#define MOTOR_ON_2_ResetPullup()        do { WPUBbits.WPUB2 = 0; } while(0)
#define MOTOR_ON_2_SetAnalogMode()      do { ANSELBbits.ANSB2 = 1; } while(0)
#define MOTOR_ON_2_SetDigitalMode()     do { ANSELBbits.ANSB2 = 0; } while(0)

// get/set MOTOR_ON_3 aliases
#define MOTOR_ON_3_TRIS                 TRISBbits.TRISB3
#define MOTOR_ON_3_LAT                  LATBbits.LATB3
#define MOTOR_ON_3_PORT                 PORTBbits.RB3
#define MOTOR_ON_3_WPU                  WPUBbits.WPUB3
#define MOTOR_ON_3_ANS                  ANSELBbits.ANSB3
#define MOTOR_ON_3_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define MOTOR_ON_3_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define MOTOR_ON_3_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define MOTOR_ON_3_GetValue()           PORTBbits.RB3
#define MOTOR_ON_3_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define MOTOR_ON_3_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define MOTOR_ON_3_SetPullup()          do { WPUBbits.WPUB3 = 1; } while(0)
#define MOTOR_ON_3_ResetPullup()        do { WPUBbits.WPUB3 = 0; } while(0)
#define MOTOR_ON_3_SetAnalogMode()      do { ANSELBbits.ANSB3 = 1; } while(0)
#define MOTOR_ON_3_SetDigitalMode()     do { ANSELBbits.ANSB3 = 0; } while(0)

// get/set SCL1 aliases
#define SCL1_TRIS                 TRISCbits.TRISC3
#define SCL1_LAT                  LATCbits.LATC3
#define SCL1_PORT                 PORTCbits.RC3
#define SCL1_ANS                  ANSELCbits.ANSC3
#define SCL1_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define SCL1_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define SCL1_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define SCL1_GetValue()           PORTCbits.RC3
#define SCL1_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define SCL1_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define SCL1_SetAnalogMode()      do { ANSELCbits.ANSC3 = 1; } while(0)
#define SCL1_SetDigitalMode()     do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set SDA1 aliases
#define SDA1_TRIS                 TRISCbits.TRISC4
#define SDA1_LAT                  LATCbits.LATC4
#define SDA1_PORT                 PORTCbits.RC4
#define SDA1_ANS                  ANSELCbits.ANSC4
#define SDA1_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define SDA1_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define SDA1_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define SDA1_GetValue()           PORTCbits.RC4
#define SDA1_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define SDA1_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define SDA1_SetAnalogMode()      do { ANSELCbits.ANSC4 = 1; } while(0)
#define SDA1_SetDigitalMode()     do { ANSELCbits.ANSC4 = 0; } while(0)

// get/set POWER_ON_LED aliases
#define POWER_ON_LED_TRIS                 TRISDbits.TRISD0
#define POWER_ON_LED_LAT                  LATDbits.LATD0
#define POWER_ON_LED_PORT                 PORTDbits.RD0
#define POWER_ON_LED_ANS                  ANSELDbits.ANSD0
#define POWER_ON_LED_SetHigh()            do { LATDbits.LATD0 = 1; } while(0)
#define POWER_ON_LED_SetLow()             do { LATDbits.LATD0 = 0; } while(0)
#define POWER_ON_LED_Toggle()             do { LATDbits.LATD0 = ~LATDbits.LATD0; } while(0)
#define POWER_ON_LED_GetValue()           PORTDbits.RD0
#define POWER_ON_LED_SetDigitalInput()    do { TRISDbits.TRISD0 = 1; } while(0)
#define POWER_ON_LED_SetDigitalOutput()   do { TRISDbits.TRISD0 = 0; } while(0)
#define POWER_ON_LED_SetAnalogMode()      do { ANSELDbits.ANSD0 = 1; } while(0)
#define POWER_ON_LED_SetDigitalMode()     do { ANSELDbits.ANSD0 = 0; } while(0)

// get/set COMP_ON_LED aliases
#define COMP_ON_LED_TRIS                 TRISDbits.TRISD1
#define COMP_ON_LED_LAT                  LATDbits.LATD1
#define COMP_ON_LED_PORT                 PORTDbits.RD1
#define COMP_ON_LED_ANS                  ANSELDbits.ANSD1
#define COMP_ON_LED_SetHigh()            do { LATDbits.LATD1 = 1; } while(0)
#define COMP_ON_LED_SetLow()             do { LATDbits.LATD1 = 0; } while(0)
#define COMP_ON_LED_Toggle()             do { LATDbits.LATD1 = ~LATDbits.LATD1; } while(0)
#define COMP_ON_LED_GetValue()           PORTDbits.RD1
#define COMP_ON_LED_SetDigitalInput()    do { TRISDbits.TRISD1 = 1; } while(0)
#define COMP_ON_LED_SetDigitalOutput()   do { TRISDbits.TRISD1 = 0; } while(0)
#define COMP_ON_LED_SetAnalogMode()      do { ANSELDbits.ANSD1 = 1; } while(0)
#define COMP_ON_LED_SetDigitalMode()     do { ANSELDbits.ANSD1 = 0; } while(0)

// get/set ALARM_ON_LED aliases
#define ALARM_ON_LED_TRIS                 TRISDbits.TRISD2
#define ALARM_ON_LED_LAT                  LATDbits.LATD2
#define ALARM_ON_LED_PORT                 PORTDbits.RD2
#define ALARM_ON_LED_ANS                  ANSELDbits.ANSD2
#define ALARM_ON_LED_SetHigh()            do { LATDbits.LATD2 = 1; } while(0)
#define ALARM_ON_LED_SetLow()             do { LATDbits.LATD2 = 0; } while(0)
#define ALARM_ON_LED_Toggle()             do { LATDbits.LATD2 = ~LATDbits.LATD2; } while(0)
#define ALARM_ON_LED_GetValue()           PORTDbits.RD2
#define ALARM_ON_LED_SetDigitalInput()    do { TRISDbits.TRISD2 = 1; } while(0)
#define ALARM_ON_LED_SetDigitalOutput()   do { TRISDbits.TRISD2 = 0; } while(0)
#define ALARM_ON_LED_SetAnalogMode()      do { ANSELDbits.ANSD2 = 1; } while(0)
#define ALARM_ON_LED_SetDigitalMode()     do { ANSELDbits.ANSD2 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/