/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.78
        Device            :  PIC18F45K22
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "mcc_generated_files/mcc.h"
#include "lcd_drivers.h"

/*
                         Main application
 */
enum MAJOR_STATE {INIT, ON, OFF, ALARM, DISP_RUN, DISP_FP, DISP_ALARM, DISP_OFF, FINALP};
enum ALARM_STATE {ESTOP, OILP};
static enum MAJOR_STATE state;
static enum ALARM_STATE aState;

typedef union inputs {
    struct {
        uint8_t finalP: 1;
        uint8_t oilP:   1;
        uint8_t reset:  1;
        uint8_t e_STOP: 1;
        uint8_t OFF:    1;
        uint8_t ON :    1;
        uint8_t X1 :    1;
        uint8_t X2 :    1;   
    }inny;
    uint8_t value;
}StatusReg;

void ISR1_define(){
    mssp1_clearIRQ();
}

void ISR_BC(){
    i2c1_driver_clearBusCollision();
}

interruptHandler ISR1 = &ISR1_define;
interruptHandler ISRbc = &ISR_BC;

/*
                         Main application
 */
void main(void)
{
    // Initialize the device
    SYSTEM_Initialize();
    
    INTERRUPT_GlobalInterruptEnable();
    
    INTERRUPT_PeripheralInterruptEnable();
    
    i2cInit();
    
    i2c1_driver_setBusCollisionISR(ISRbc);
    i2c1_driver_setI2cISR(ISR1);
    
    LCDInit();
    
    char initMsg[] = "Power On";
    char stbyMsg[] = "Off";
    char runMsg[] = "Running";
    char ESMsg[] = "ESD Alarm!";
    char fpMsg[] = "Standby...";
    char opMsg[] = "OP Alarm";
    
    writeString(&initMsg);

    StatusReg InReg;
    
    state = DISP_OFF;
    
    __delay_ms(1000);
    
    POWER_ON_LED_SetHigh();

    while (1)
    {          
        
    InReg.inny.finalP = FINALP_SW_GetValue();  
    InReg.inny.oilP = OILP_SW_GetValue();
    InReg.inny.reset = RESET_SW_GetValue(); 
    InReg.inny.e_STOP = ESTOP_SW_GetValue(); 
    InReg.inny.OFF = OFF_SW_GetValue(); 
    InReg.inny.ON = ON_SW_GetValue();
   
        switch(state){
            case DISP_RUN:
                clearScreen();
                writeString(&runMsg);
                state = ON;
                break;
            case ON:
                COMP_ON_LED_SetHigh(); //turn on
                ALARM_ON_LED_SetLow();
                if(InReg.inny.finalP == 0){
                    state = DISP_FP;
                }
                else if(InReg.inny.OFF == 1){
                    state = DISP_OFF;
                }else if(InReg.inny.e_STOP == 0){
                    state = DISP_ALARM;
                    aState = ESTOP;
                }else if(InReg.inny.oilP == 0){
                    state = DISP_ALARM;
                    aState = OILP;
                }else{
                    MOTOR_ON_1_SetHigh(); //chasing LEDs for motor
                    __delay_ms(50);
                    MOTOR_ON_2_SetHigh();      
                    MOTOR_ON_1_SetLow();      
                    __delay_ms(50);
                    MOTOR_ON_3_SetHigh();      
                    MOTOR_ON_2_SetLow();      
                    __delay_ms(50);
                    MOTOR_ON_3_SetLow();
                    }
                break;
            case DISP_FP:
                clearScreen();
                writeString(&fpMsg);
                state = FINALP;
                break;
            case FINALP:
                if(InReg.inny.finalP == 1){
                    state = DISP_RUN;
                }else{
                    MOTOR_ON_1_SetLow();
                    MOTOR_ON_2_SetLow();
                    MOTOR_ON_3_SetLow();
                } 
                break;
            case DISP_OFF:
                clearScreen();
                writeString(&stbyMsg);
                state = OFF;
                break;
            case OFF:
                ALARM_ON_LED_SetLow();
                if(InReg.inny.e_STOP == 0){
                    state = DISP_ALARM;
                    aState = ESTOP;
                }else if(InReg.inny.ON == 1){
                    state = DISP_RUN;
                }else{
                COMP_ON_LED_SetLow();
                MOTOR_ON_1_SetLow();
                MOTOR_ON_2_SetLow();
                MOTOR_ON_3_SetLow();
                }
                break;
            case DISP_ALARM:
                clearScreen();
                if(aState == ESTOP){
                    writeString(&ESMsg);
                }
                else{
                    writeString(&opMsg);
                }
                state = ALARM;
                break;
            case ALARM:
                COMP_ON_LED_SetLow();
                if(InReg.inny.reset == 1){
                    state = DISP_OFF;
                }else{
                    switch(aState){
                        case ESTOP:
                            ALARM_ON_LED_Toggle();
                            __delay_ms(500);
                            break;
                        case OILP:
                            ALARM_ON_LED_Toggle();
                            __delay_ms(250);
                            break;
                    }
                    break;
                }
            default: //similar to OFF state
                POWER_ON_LED_SetHigh();
                break;            
        }
          
    }
}
/**
 End of File
*/