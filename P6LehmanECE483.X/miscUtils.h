/* 
 * File:   miscUtils.h
 * Author: micah
 *
 * Created on November 27, 2019, 9:30 PM
 */

#ifndef MISCUTILS_H
#define	MISCUTILS_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include "mcc_generated_files/adc.h"

    void initMvgAvg(uint8_t n);


#ifdef	__cplusplus
}
#endif

#endif	/* MISCUTILS_H */

