/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.78
        Device            :  PIC18F45K22
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

// PCF8574 register and pin mapping
// Bit 0 - RS  - P0 - Pin 4  PCF8574
// Bit 1 - RW  - P1 - Pin 5  PCF8574
// Bit 2 - En  - P2 - Pin 6  PCF8574
// Bit 3 - Led - P3 - Pin 7  PCF8574 (Active High, Led turned on)
// Bit 4 - D4  - P4 - Pin 9  PCF8574
// Bit 5 - D5  - P5 - Pin 10 PCF8574
// Bit 6 - D6  - P6 - Pin 11 PCF8574
// Bit 7 - D7  - P7 - Pin 12 PCF8574

#include "mcc_generated_files/mcc.h"
#include "lcd_drivers.h"
#include <stdlib.h>

#define ADDR 0x4e
#define BL   0x08
#define EN   0x04
#define RW   0x02
#define RS   0x01

#define AVG_RES 50
/*
                         Main application
 */

void ISR1_define(){
    mssp2_clearIRQ();
    DBG_LED_Toggle();
}

void ISR_BC(){
    i2c2_driver_clearBusCollision();
}
    interruptHandler ISR1 = &ISR1_define;
    interruptHandler ISRbc = &ISR_BC;

void main(void)
{
    // Initialize the device
    SYSTEM_Initialize();

    // If using interrupts in PIC18 High/Low Priority Mode you need to enable the Global High and Low Interrupts
    // If using interrupts in PIC Mid-Range Compatibility Mode you need to enable the Global and Peripheral Interrupts
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();
  
    i2cInit();
    ADC_Initialize();
 
    i2c2_driver_setBusCollisionISR(ISRbc);
    i2c2_driver_setI2cISR(ISR1);
    
    mssp2_enableIRQ();
    
    LCDInit();
    
    char hw_str[] = "Hello World!";
    
    writeString(hw_str);
    
    __delay_ms(3000);
    
    clearScreen();
    
    char finalPStr[] = "FpV: ";
    char oilPStr[] = "OpV: ";
    
    writeString(finalPStr);
    cursorPos(0x00, 2);
    writeString(oilPStr);
    
    adc_result_t fpVal, opVal;
    float fpOUT, opOUT;
    char *opValStr;
    char *fpValStr;
    
    int opStat, fpStat, mvgAvg, mvgAvg2;
    
    int mvgSum = 0;
    int mvgSum2 = 0;
    
    

    while (1)
    {
//        fpVal = ADC_GetConversion(FINAL_P_INPUT);
//        
//        mvgSum+=mvgSum;
//        
//        mvgAvg = mvgSum/AVG_RES;
//        
//        fpOUT = 5.0 * ((float)fpVal/1024.0);
//        
//        fpValStr = ftoa(fpOUT, fpStat);
//        
//        cursorPos(0x05, 1);
//        writeString(fpValStr);
//        
//        opVal = ADC_GetConversion(OIL_P_INPUT);
//        
//        mvgSum2 += mvgSum;
//        
//        mvgAvg2 = mvgSum2/AVG_RES;
//        
//        opOUT = 5.0 * ((float)opVal/1024.0);
//        
//        opValStr = ftoa(opOUT, opStat);
//        
//        cursorPos(0x05, 2);
//        writeString(opValStr);
           // Add your application code
        //read ADC inputs, display final pressure on top, oil pressure on bottom

        fpVal = ADC_GetConversion(FINAL_P_INPUT);
        
        fpOUT = 5.0 * ((float)fpVal/1024.0);
        
        fpValStr = ftoa(fpOUT, fpStat);
        
        cursorPos(0x05, 1);
        writeString(fpValStr);

        opVal = ADC_GetConversion(OIL_P_INPUT);
        
        opOUT = 5.0 * ((float)opVal/1024.0);
        
        opValStr = ftoa(opOUT, opStat);
        
        cursorPos(0x05, 2);
        writeString(opValStr);
    }
}
/**
 End of File
*/