/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.78
        Device            :  PIC18F45K22
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.05 and above
        MPLAB 	          :  MPLAB X 5.20	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set OIL_P_INPUT aliases
#define OIL_P_INPUT_TRIS                 TRISAbits.TRISA2
#define OIL_P_INPUT_LAT                  LATAbits.LATA2
#define OIL_P_INPUT_PORT                 PORTAbits.RA2
#define OIL_P_INPUT_ANS                  ANSELAbits.ANSA2
#define OIL_P_INPUT_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define OIL_P_INPUT_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define OIL_P_INPUT_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define OIL_P_INPUT_GetValue()           PORTAbits.RA2
#define OIL_P_INPUT_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define OIL_P_INPUT_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define OIL_P_INPUT_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define OIL_P_INPUT_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set FINAL_P_INPUT aliases
#define FINAL_P_INPUT_TRIS                 TRISAbits.TRISA3
#define FINAL_P_INPUT_LAT                  LATAbits.LATA3
#define FINAL_P_INPUT_PORT                 PORTAbits.RA3
#define FINAL_P_INPUT_ANS                  ANSELAbits.ANSA3
#define FINAL_P_INPUT_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define FINAL_P_INPUT_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define FINAL_P_INPUT_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define FINAL_P_INPUT_GetValue()           PORTAbits.RA3
#define FINAL_P_INPUT_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define FINAL_P_INPUT_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define FINAL_P_INPUT_SetAnalogMode()      do { ANSELAbits.ANSA3 = 1; } while(0)
#define FINAL_P_INPUT_SetDigitalMode()     do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set SCL2 aliases
#define SCL2_TRIS                 TRISDbits.TRISD0
#define SCL2_LAT                  LATDbits.LATD0
#define SCL2_PORT                 PORTDbits.RD0
#define SCL2_ANS                  ANSELDbits.ANSD0
#define SCL2_SetHigh()            do { LATDbits.LATD0 = 1; } while(0)
#define SCL2_SetLow()             do { LATDbits.LATD0 = 0; } while(0)
#define SCL2_Toggle()             do { LATDbits.LATD0 = ~LATDbits.LATD0; } while(0)
#define SCL2_GetValue()           PORTDbits.RD0
#define SCL2_SetDigitalInput()    do { TRISDbits.TRISD0 = 1; } while(0)
#define SCL2_SetDigitalOutput()   do { TRISDbits.TRISD0 = 0; } while(0)
#define SCL2_SetAnalogMode()      do { ANSELDbits.ANSD0 = 1; } while(0)
#define SCL2_SetDigitalMode()     do { ANSELDbits.ANSD0 = 0; } while(0)

// get/set SDA2 aliases
#define SDA2_TRIS                 TRISDbits.TRISD1
#define SDA2_LAT                  LATDbits.LATD1
#define SDA2_PORT                 PORTDbits.RD1
#define SDA2_ANS                  ANSELDbits.ANSD1
#define SDA2_SetHigh()            do { LATDbits.LATD1 = 1; } while(0)
#define SDA2_SetLow()             do { LATDbits.LATD1 = 0; } while(0)
#define SDA2_Toggle()             do { LATDbits.LATD1 = ~LATDbits.LATD1; } while(0)
#define SDA2_GetValue()           PORTDbits.RD1
#define SDA2_SetDigitalInput()    do { TRISDbits.TRISD1 = 1; } while(0)
#define SDA2_SetDigitalOutput()   do { TRISDbits.TRISD1 = 0; } while(0)
#define SDA2_SetAnalogMode()      do { ANSELDbits.ANSD1 = 1; } while(0)
#define SDA2_SetDigitalMode()     do { ANSELDbits.ANSD1 = 0; } while(0)

// get/set DBG_LED aliases
#define DBG_LED_TRIS                 TRISDbits.TRISD2
#define DBG_LED_LAT                  LATDbits.LATD2
#define DBG_LED_PORT                 PORTDbits.RD2
#define DBG_LED_ANS                  ANSELDbits.ANSD2
#define DBG_LED_SetHigh()            do { LATDbits.LATD2 = 1; } while(0)
#define DBG_LED_SetLow()             do { LATDbits.LATD2 = 0; } while(0)
#define DBG_LED_Toggle()             do { LATDbits.LATD2 = ~LATDbits.LATD2; } while(0)
#define DBG_LED_GetValue()           PORTDbits.RD2
#define DBG_LED_SetDigitalInput()    do { TRISDbits.TRISD2 = 1; } while(0)
#define DBG_LED_SetDigitalOutput()   do { TRISDbits.TRISD2 = 0; } while(0)
#define DBG_LED_SetAnalogMode()      do { ANSELDbits.ANSD2 = 1; } while(0)
#define DBG_LED_SetDigitalMode()     do { ANSELDbits.ANSD2 = 0; } while(0)

// get/set DBG_LED2 aliases
#define DBG_LED2_TRIS                 TRISDbits.TRISD3
#define DBG_LED2_LAT                  LATDbits.LATD3
#define DBG_LED2_PORT                 PORTDbits.RD3
#define DBG_LED2_ANS                  ANSELDbits.ANSD3
#define DBG_LED2_SetHigh()            do { LATDbits.LATD3 = 1; } while(0)
#define DBG_LED2_SetLow()             do { LATDbits.LATD3 = 0; } while(0)
#define DBG_LED2_Toggle()             do { LATDbits.LATD3 = ~LATDbits.LATD3; } while(0)
#define DBG_LED2_GetValue()           PORTDbits.RD3
#define DBG_LED2_SetDigitalInput()    do { TRISDbits.TRISD3 = 1; } while(0)
#define DBG_LED2_SetDigitalOutput()   do { TRISDbits.TRISD3 = 0; } while(0)
#define DBG_LED2_SetAnalogMode()      do { ANSELDbits.ANSD3 = 1; } while(0)
#define DBG_LED2_SetDigitalMode()     do { ANSELDbits.ANSD3 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/