/* 
 * File:   myi2c.h
 * Author: micah
 *
 * Created on November 11, 2019, 6:05 PM
 */

#ifndef MYI2C_H
#define	MYI2C_H

#include "mcc_generated_files/i2c2_driver.h"
#include "mcc_generated_files/pin_manager.h"

#ifdef	__cplusplus
extern "C" {
#endif

    void i2cStart();
    void i2cStop();
    void i2cRestart();
    void i2cIdle();
    void i2cWriteByte(char);
    void i2cWriteTransaction(char, char);
    void i2cInit();


#ifdef	__cplusplus
}
#endif

#endif	/* MYI2C_H */

