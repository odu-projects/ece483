# ECE 483 Project 6  

Copy of project 4 (i2c lcd) with ADC added.

Check out [Microchip Developer Help](https://microchipdeveloper.com/8bit:emr-adc) for a good getting started and link to an example project.

Project 6 pin config:   
![](p6setup.png)
