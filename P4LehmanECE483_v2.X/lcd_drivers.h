/* 
 * File:   lcd_drivers.h
 * Author: micah
 *
 * Created on November 11, 2019, 7:17 PM
 */

#ifndef LCD_DRIVERS_H
#define	LCD_DRIVERS_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "myi2c.h"
#include "mcc_generated_files/device_config.h"

    void pulseEnableNegative(char);
    void writeNib(char);
    void writeString(char*);
    void LCDInit(void);
    void LCDSend(uint8_t, uint8_t);

#ifdef	__cplusplus
}
#endif

#endif	/* LCD_DRIVERS_H */

