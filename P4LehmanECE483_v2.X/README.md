# ECE 483 Project 4 v2  

A lot of this code is based on a library I found online that is in turn  
based on the arduino i2c lcd library. The tricks are making sure the i2c bus  
is idle and how to pulse the enable bit. A negative edge on the enable bit  
latches data in. Note how the code handles it.

Links:

[Arduino i2c lcd library](https://www.arduinolibraries.info/libraries/liquid-crystal-i2-c)
- Its version 1.1.2

[pic i2c lcd driver](https://www.instructables.com/id/Microchip-PIC-Library-to-Control-a-20-by-4-LCD-ove/)
- At nearly the bottom of the page is a download for a zip file. You will  
 see the similarities between it and the arduino library.  
 
[Microchip developer help pseudocode for i2c write transaction](https://microchipdeveloper.com/i2c:byte-write)  

project pin config:  

![](p4setup.png)
