#include <pic18f45k22.h>

#include "myi2c.h"


void i2cInit(){
    if(i2c2_driver_open()){
        //DBG_LED_Toggle();
    }else{
        i2c2_driver_open();
    }
}

void i2cIdle(){
    //while(i2c2_driver_isStart()||i2c2_driver_isStop()||i2c2_driver_isRestart()||i2c2_driver_isReceive()||i2c2_driver_isRead());
    while(SSP2CON2 & 0x1F || SSP2STATbits.R_NOT_W);
}

void i2cWriteTransaction(char addr, char data){
    i2cIdle();
    i2c2_driver_start();
    i2cIdle();
    i2c2_driver_TXData(addr);
    i2cIdle();
    i2c2_driver_TXData(data);
    i2cIdle();
    i2c2_driver_stop();
}

