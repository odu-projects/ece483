/* 
 * File:   myi2c.h
 * Author: micah
 *
 * Created on November 17, 2019, 8:38 PM
 */

#ifndef MYI2C_H
#define	MYI2C_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "mcc_generated_files/i2c1_driver.h"
#include "mcc_generated_files/pin_manager.h"

    void i2cStart();
    void i2cStop();
    void i2cRestart();
    void i2cIdle();
    void i2cWriteByte(char);
    void i2cWriteTransaction(char, char);
    void i2cInit();

#ifdef	__cplusplus
}
#endif

#endif	/* MYI2C_H */

