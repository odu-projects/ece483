/* 
 * File:   utilities.h
 * Author: micah
 *
 * Created on December 4, 2019, 6:09 PM
 */

#ifndef UTILITIES_H
#define	UTILITIES_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>

    char *val2Char(uint32_t ratio, char* returnBuf);
    char int2char(int);

#ifdef	__cplusplus
}
#endif

#endif	/* UTILITIES_H */

