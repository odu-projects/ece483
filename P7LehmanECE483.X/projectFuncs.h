/* 
 * File:   projectFuncs.h
 * Author: micah
 *
 * Created on December 9, 2019, 5:40 PM
 */

#ifndef PROJECTFUNCS_H
#define	PROJECTFUNCS_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "lcd_drivers.h"

    void displayPowerMsg(void);
    void displayOffMsg(void);
    void displayRunMsg(void);
    void displayESMsg(void);
    void displayFPMsg(void);
    void displayOPMsg(void);
    void displayOPWaitMsg(void);


#ifdef	__cplusplus
}
#endif

#endif	/* PROJECTFUNCS_H */

