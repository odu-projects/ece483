#include "projectFuncs.h"

    char initMsg[] = "Power On";
    char stbyMsg[] = "Off";
    char runMsg[] = "Running";
    char ESMsg[] = "ES Alarm!";
    char fpMsg[] = "Standby...";
    char opMsg[] = "OP Alarm";
    char opWtMsg[] = "OP detected...";

void displayPowerMsg(void){
    cursorPos(0x00, 1);
    writeString(initMsg);
}

void displayOffMsg(void){
    cursorPos(0x00, 1); 
    writeString(stbyMsg);
}

void displayRunMsg(void){
    cursorPos(0x00, 1); 
    writeString(runMsg);
}

void displayESMsg(void){
    cursorPos(0x00, 1);
    writeString(ESMsg);
}

void displayFPMsg(void){
    cursorPos(0x00, 1);                
    writeString(fpMsg);
}

void displayOPMsg(void){
    cursorPos(0x00, 1);
    writeString(opMsg);
}

void displayOPWaitMsg(void){
    cursorPos(0x00, 1);
    writeString(opWtMsg);
}

