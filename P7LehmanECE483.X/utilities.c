#include "utilities.h"

/*
 * ADC resolves value to 10 bits, will be stored in a 16 bit unsigned
 */

char int2char(int val){
    char returnTemp;
    switch (val){
        case 1:
            returnTemp = '1';
            break;
        case 2:
            returnTemp = '2';
            break;
        case 3:
            returnTemp = '3';
            break;
        case 4:
            returnTemp = '4';
            break;
        case 5:
            returnTemp = '5';
            break;
        case 6:
            returnTemp = '6';
            break;
        case 7:
            returnTemp = '7';
            break;
        case 8:
            returnTemp = '8';
            break;
        case 9:
            returnTemp = '9';
            break;
        case 0:
            returnTemp = '0';
            break;
        default:
            returnTemp = '$';
            break;
    }
    
    return returnTemp;
}

char *val2Char(uint32_t ratio, char * returnBuf){
    int ones = ratio % 10;
    int tens = (ratio % 100)/10;
    int hundo = (ratio % 1000)/100;
    int thous = ratio/1000;
    
    
    returnBuf[0] = int2char(thous);
    returnBuf[1] = int2char(hundo);
    returnBuf[2] = int2char(tens);
    returnBuf[3] = int2char(ones);
    returnBuf[4] = '\0';
//    returnBuf[0] = int2char(ones);
//    returnBuf[1] = int2char(tens);
//    returnBuf[2] = int2char(hundo);
//    returnBuf[3] = int2char(thous);
    //returnBuf[4] = '\0';
    
    return returnBuf;
}

