#include "lcd_drivers.h"

#define EN      0x04
#define ADDR    0x4e
#define RW      0x02
#define RS      0x01
#define BL      0x08

char clearL[] = "                  ";

//LCD is some variation of the me-too type- Hitachi HD44780 16x2
//theres a link to the data sheet in project4 folder I think
//as with the other projects, the only difference is the addition of the
//i2c backpack

void pulseEnableNegative(char data){
    i2cWriteTransaction(ADDR, data | EN);
    __delay_us(1);
    i2cWriteTransaction(ADDR, data & ~EN);
    __delay_us(50); //probably these delays aren't necessary
}

void writeNib(char nibs){
    i2cWriteTransaction(ADDR, nibs|BL);
    pulseEnableNegative(nibs);
}

void LCDSend(uint8_t data, uint8_t Rs){
    uint8_t hiNib = data & 0xF0;
    uint8_t loNib = (data << 4) & 0xF0;
    
    writeNib(hiNib | EN | Rs | BL);
    writeNib(loNib | EN | Rs | BL);
    __delay_us(50);
}

void writeString(char *c){
    uint8_t *msg_ptr = (uint8_t *)c;  
    while(*msg_ptr)
        LCDSend((uint8_t)(*msg_ptr++), RS);
}

void writeChar(char c){
    LCDSend(c, RS);
}

void clearScreen(void){
    LCDSend(0x01, 0x00);
    __delay_us(2000);
}

void cursorPos(uint8_t pos, uint8_t line){
    //positions 0-f, lines 1 or 2
    uint8_t posVal;
    if(line == 2){
        posVal = pos | 0x40;
    }else if(line == 1){
        posVal = pos | 0x00;
    }
    posVal = posVal | 0x80;
    LCDSend(posVal, 0x00);    
}

void clearLine1(void){
    cursorPos(0x00, 1);
    writeString(clearL);
}

void clearLine2(void){
    cursorPos(0x00, 2);
    writeString(clearL); 
}

void LCDInit(void){
    __delay_ms(60);   
    writeNib(0x20);  
    writeNib(0x00); //pull everything to zero   
    __delay_ms(1000);    
    writeNib(0x30);   
    __delay_us(4500);   
    writeNib(0x30);   
    __delay_us(4500);  
    writeNib(0x30);   
    __delay_us(150);  
    writeNib(0x20); //set 4 bit       
    writeNib(0x20);    
    writeNib(0xc0);  
    writeNib(0x00);    
    writeNib(0xc0);  // modified to turn off cursor (looks way better) 
    writeNib(0x00);    
    writeNib(0x10);// not sure why I don't need a zero after this   
    writeNib(0x70);
}
