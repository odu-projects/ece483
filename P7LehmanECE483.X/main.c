/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.78
        Device            :  PIC18F45K22
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "mcc_generated_files/mcc.h"
#include "projectFuncs.h"
#include "utilities.h"
/*
                         Main application
 */
enum MAJOR_STATE {ON, OFF, OILP, ESTOP,  DISP_RUN, DISP_FP, DISP_OILP, DISP_ESTOP, DISP_OFF, FINALP, OILP_WT};
volatile enum MAJOR_STATE state;

typedef union inputs {
    struct {
        uint8_t unused1: 1;
        uint8_t unused2: 1;
        uint8_t reset:  1;
        uint8_t e_STOP: 1;
        uint8_t OFF:    1;
        uint8_t ON :    1;
        uint8_t X1 :    1;
        uint8_t X2 :    1;   
    }inny;
    uint8_t value;
}StatusReg;

void ISR1_define(){
    mssp1_clearIRQ();
}

void ISR_BC(){
    i2c1_driver_clearBusCollision();
}

interruptHandler ISR1 = &ISR1_define;
interruptHandler ISRbc = &ISR_BC;

volatile bool opReady = false;
char opChar;
volatile int opCount = 0; //timer1 isr fires every second, will count 10,
                            //on tenth, op alarm begins
void oilPressureAlarm(){
    //10 second countdown after oil pressure event triggered    
    if(opCount == 10){
        opCount = 0;
        opReady = true;
        TMR1_StopTimer();
    }else{
        opCount++;
    }
}

/*
                         Main application
 */
void main(void)
{
    // Initialize the device
    SYSTEM_Initialize();
    
    INTERRUPT_GlobalInterruptEnable();
    
    INTERRUPT_PeripheralInterruptEnable();
    
    i2cInit();
    
    i2c1_driver_setBusCollisionISR(ISRbc);
    i2c1_driver_setI2cISR(ISR1);
    
    LCDInit();
    
    ADC_Initialize();
    TMR1_Initialize();
    TMR1_StopTimer(); //without this timer begins right away!
    
    TMR1_SetInterruptHandler(oilPressureAlarm);
    
    uint32_t fpVal, opVal;
    char* fpDisp;
    char* opDisp;
    
    char buf[7]; //note 16x2 lcd display format
    
    char fp[] = "FP: ";
    char op[] = "OP: ";
    
    displayPowerMsg();
    
    cursorPos(0x00, 2);
    writeString(fp);
    cursorPos(0x09, 2);
    writeString(op);
    
    __delay_ms(5000);

    StatusReg InReg; //instantiate register
    
    state = DISP_OFF;
    
    __delay_ms(1000);
    
    POWER_ON_LED_SetHigh();

    while (1)
    {          
    InReg.inny.reset = RESET_SW_GetValue(); 
    InReg.inny.e_STOP = ESTOP_SW_GetValue(); 
    InReg.inny.OFF = OFF_SW_GetValue(); 
    InReg.inny.ON = ON_SW_GetValue();
   
    //value display state must be up here somewhere
    //=============ADC STUFF==========================
    fpVal = ADC_GetConversion(FINALP_AN3);
    
    fpVal = (5000 * fpVal)>>10; //5000 times ratio, divide by 1024
    
    fpDisp = val2Char(fpVal, buf);
    
    cursorPos(0x03, 2);
    writeString(fpDisp);
    
    opVal = ADC_GetConversion(OILP_AN2); //floats- who needs em
    
    opVal = ((1000 * opVal)>>10)/10;
    
    opDisp = val2Char(opVal, buf);
    
    cursorPos(0x0c, 2);
    writeString(opDisp);
    //==============end ADC stuff===================
    //==============OP INIT TIMER===================
    cursorPos(0x0f, 1);
    opChar = int2char(opCount);
    writeChar(opChar);
    //==============================================
       
        switch(state){
            case DISP_RUN:
                clearLine1();
                displayRunMsg();
                cursorPos(0x0f, 1);
                writeChar(' ');
                opCount = 0;
                TMR1_StartTimer(); //begin oilP count
                state = ON;
                break;
            case ON:
                COMP_ON_LED_SetHigh(); //turn on
                ALARM_ON_LED_SetLow();
                if(fpVal >= 4000){ 
                    state = DISP_FP;
                }else if(InReg.inny.OFF == 1){
                    state = DISP_OFF;
                }else if(InReg.inny.e_STOP == 0){
                    state = DISP_ESTOP;
                }else if(opVal < 50 || opVal > 80){
                    if(opReady == true){
                        state = DISP_OILP;
                        clearLine1();
                        displayOPWaitMsg();
                    }else{
                        MOTOR_ON_1_SetHigh(); //chasing LEDs for motor
                        __delay_ms(10);
                        MOTOR_ON_2_SetHigh();      
                        MOTOR_ON_1_SetLow();      
                        __delay_ms(10);
                        MOTOR_ON_3_SetHigh();      
                        MOTOR_ON_2_SetLow();      
                        __delay_ms(10);
                        MOTOR_ON_3_SetLow();
                    }
                }else{
                    MOTOR_ON_1_SetHigh(); //chasing LEDs for motor
                    __delay_ms(10);
                    MOTOR_ON_2_SetHigh();      
                    MOTOR_ON_1_SetLow();      
                    __delay_ms(10);
                    MOTOR_ON_3_SetHigh();      
                    MOTOR_ON_2_SetLow();      
                    __delay_ms(10);
                    MOTOR_ON_3_SetLow();
                    }
                break;
            case DISP_FP:
                clearLine1();
                displayFPMsg();
                state = FINALP;
                break;
            case FINALP:
                if(fpVal < 3000){
                    state = DISP_RUN;
                }else{
                    MOTOR_ON_1_SetLow();
                    MOTOR_ON_2_SetLow();
                    MOTOR_ON_3_SetLow();
                } 
                break;
            case DISP_OFF:
                clearLine1();
                displayOffMsg();
                state = OFF;
                break;
            case OFF:
                ALARM_ON_LED_SetLow();
                if(InReg.inny.e_STOP == 0){
                    state = DISP_ESTOP;
                }else if(InReg.inny.ON == 1 && fpVal ){
                    state = DISP_RUN;
                }else{
                COMP_ON_LED_SetLow();
                MOTOR_ON_1_SetLow();
                MOTOR_ON_2_SetLow();
                MOTOR_ON_3_SetLow();
                }
                break;
            case DISP_ESTOP:
                clearLine1();
                displayESMsg();
                state = ESTOP;
                break;
            case ESTOP:
                COMP_ON_LED_SetLow();
                if(InReg.inny.reset == 1){
                    state = DISP_OFF;
                }
                ALARM_ON_LED_Toggle();
                __delay_ms(500);
                break;
            case DISP_OILP:
                clearLine1();
                displayOPMsg();
                cursorPos(0x0f, 1);
                writeChar(' ');
                state = OILP;
                break;
            case OILP:
                COMP_ON_LED_SetLow();
                if(InReg.inny.reset == 1){
                    state = DISP_OFF;
                }
                ALARM_ON_LED_Toggle();
                __delay_ms(250);
                break;
            default: //similar to OFF state
                POWER_ON_LED_SetHigh();
                break;            
        }
          
    }
}
/**
 End of File
*/