#include "counter_funcs.h"

typedef union port {
    struct {
        uint8_t a:  1; //LSB!!!
        uint8_t b:  1;
        uint8_t c:  1;
        uint8_t d:  1;
        uint8_t e:  1;
        uint8_t f:  1;
        uint8_t g:  1;
        uint8_t dp: 1;   
    }write;
    uint8_t value;
}digitPort;

 //instantiate DIGIT regs

void testDigits(void){  //rapidly shift through all segments
    digitPort shifter;
    shifter.value = 0x01; 
    
    do{
        LATC = shifter.value;
        shifter.value = shifter.value << 1;
        __delay_ms(50);
    }while(shifter.value != 0x00);
    
    shifter.value = 0x01;
    
    do{
        LATD = shifter.value;
        shifter.value = shifter.value << 1;
        __delay_ms(50);
    }while(shifter.value != 0x00);
}

int extractTENS(int val){
    int extraction;
    
    extraction = val/10;
    
    return extraction;
}

int extractONES(int val){
    int extraction;
    
    extraction = val % 10;
    
    return extraction;
}

void displayDigits(int tens, int ones){
    
    uint8_t tenVal, oneVal;
    digitPort tensDIGIT, onesDIGIT;
    
    switch(tens){
        case 0:
            tenVal = ZERO;
            break;
        case 1:
            tenVal = ONE;
            break;
        case 2:
            tenVal = TWO;
            break;
        case 3:
            tenVal = THREE;
            break;
        case 4:
            tenVal = FOUR;
            break;
        case 5:
            tenVal = FIVE;
            break;
        case 6:
            tenVal = SIX;
            break;
        case 7:
            tenVal = SEVEN;
            break;
        case 8:
            tenVal = EIGHT;
            break;
        case 9:
            tenVal = NINE;
            break;
        default:
            tenVal = ERR;
            break; //error case
    }
    
    switch(ones){
        case 0:
            oneVal = ZERO;
            break;            
        case 1:
            oneVal = ONE;
            break;
        case 2:
            oneVal = TWO;
            break;
        case 3:
            oneVal = THREE;
            break;
        case 4:
            oneVal = FOUR;
            break;
        case 5:
            oneVal = FIVE;
            break;
        case 6:
            oneVal = SIX;
            break;
        case 7:
            oneVal = SEVEN;
            break;
        case 8:
            oneVal = EIGHT;
            break;
        case 9:
            oneVal = NINE;
            break;
        default:
            oneVal = ERR;
            break;
    }
    
    tensDIGIT.value = tenVal;
    onesDIGIT.value = oneVal;
    
    LATC = tensDIGIT.value;
    LATD = onesDIGIT.value;
}
