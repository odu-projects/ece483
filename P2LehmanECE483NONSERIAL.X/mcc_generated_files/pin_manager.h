/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.77
        Device            :  PIC18F45K22
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.05 and above
        MPLAB 	          :  MPLAB X 5.20	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set ON_SW aliases
#define ON_SW_TRIS                 TRISAbits.TRISA0
#define ON_SW_LAT                  LATAbits.LATA0
#define ON_SW_PORT                 PORTAbits.RA0
#define ON_SW_ANS                  ANSELAbits.ANSA0
#define ON_SW_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define ON_SW_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define ON_SW_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define ON_SW_GetValue()           PORTAbits.RA0
#define ON_SW_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define ON_SW_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define ON_SW_SetAnalogMode()      do { ANSELAbits.ANSA0 = 1; } while(0)
#define ON_SW_SetDigitalMode()     do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set OFF_SW aliases
#define OFF_SW_TRIS                 TRISAbits.TRISA1
#define OFF_SW_LAT                  LATAbits.LATA1
#define OFF_SW_PORT                 PORTAbits.RA1
#define OFF_SW_ANS                  ANSELAbits.ANSA1
#define OFF_SW_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define OFF_SW_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define OFF_SW_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define OFF_SW_GetValue()           PORTAbits.RA1
#define OFF_SW_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define OFF_SW_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define OFF_SW_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define OFF_SW_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set UD_SW aliases
#define UD_SW_TRIS                 TRISAbits.TRISA2
#define UD_SW_LAT                  LATAbits.LATA2
#define UD_SW_PORT                 PORTAbits.RA2
#define UD_SW_ANS                  ANSELAbits.ANSA2
#define UD_SW_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define UD_SW_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define UD_SW_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define UD_SW_GetValue()           PORTAbits.RA2
#define UD_SW_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define UD_SW_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define UD_SW_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define UD_SW_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set TOG_SW aliases
#define TOG_SW_TRIS                 TRISAbits.TRISA3
#define TOG_SW_LAT                  LATAbits.LATA3
#define TOG_SW_PORT                 PORTAbits.RA3
#define TOG_SW_ANS                  ANSELAbits.ANSA3
#define TOG_SW_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define TOG_SW_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define TOG_SW_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define TOG_SW_GetValue()           PORTAbits.RA3
#define TOG_SW_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define TOG_SW_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define TOG_SW_SetAnalogMode()      do { ANSELAbits.ANSA3 = 1; } while(0)
#define TOG_SW_SetDigitalMode()     do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set RST_SW aliases
#define RST_SW_TRIS                 TRISAbits.TRISA4
#define RST_SW_LAT                  LATAbits.LATA4
#define RST_SW_PORT                 PORTAbits.RA4
#define RST_SW_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define RST_SW_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define RST_SW_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define RST_SW_GetValue()           PORTAbits.RA4
#define RST_SW_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define RST_SW_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)

// get/set TENS_a aliases
#define TENS_a_TRIS                 TRISCbits.TRISC0
#define TENS_a_LAT                  LATCbits.LATC0
#define TENS_a_PORT                 PORTCbits.RC0
#define TENS_a_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define TENS_a_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define TENS_a_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define TENS_a_GetValue()           PORTCbits.RC0
#define TENS_a_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define TENS_a_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)

// get/set TENS_b aliases
#define TENS_b_TRIS                 TRISCbits.TRISC1
#define TENS_b_LAT                  LATCbits.LATC1
#define TENS_b_PORT                 PORTCbits.RC1
#define TENS_b_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define TENS_b_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define TENS_b_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define TENS_b_GetValue()           PORTCbits.RC1
#define TENS_b_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define TENS_b_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)

// get/set TENS_c aliases
#define TENS_c_TRIS                 TRISCbits.TRISC2
#define TENS_c_LAT                  LATCbits.LATC2
#define TENS_c_PORT                 PORTCbits.RC2
#define TENS_c_ANS                  ANSELCbits.ANSC2
#define TENS_c_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define TENS_c_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define TENS_c_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define TENS_c_GetValue()           PORTCbits.RC2
#define TENS_c_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define TENS_c_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define TENS_c_SetAnalogMode()      do { ANSELCbits.ANSC2 = 1; } while(0)
#define TENS_c_SetDigitalMode()     do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set TENS_d aliases
#define TENS_d_TRIS                 TRISCbits.TRISC3
#define TENS_d_LAT                  LATCbits.LATC3
#define TENS_d_PORT                 PORTCbits.RC3
#define TENS_d_ANS                  ANSELCbits.ANSC3
#define TENS_d_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define TENS_d_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define TENS_d_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define TENS_d_GetValue()           PORTCbits.RC3
#define TENS_d_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define TENS_d_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define TENS_d_SetAnalogMode()      do { ANSELCbits.ANSC3 = 1; } while(0)
#define TENS_d_SetDigitalMode()     do { ANSELCbits.ANSC3 = 0; } while(0)

// get/set TENS_e aliases
#define TENS_e_TRIS                 TRISCbits.TRISC4
#define TENS_e_LAT                  LATCbits.LATC4
#define TENS_e_PORT                 PORTCbits.RC4
#define TENS_e_ANS                  ANSELCbits.ANSC4
#define TENS_e_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define TENS_e_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define TENS_e_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define TENS_e_GetValue()           PORTCbits.RC4
#define TENS_e_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define TENS_e_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define TENS_e_SetAnalogMode()      do { ANSELCbits.ANSC4 = 1; } while(0)
#define TENS_e_SetDigitalMode()     do { ANSELCbits.ANSC4 = 0; } while(0)

// get/set TENS_f aliases
#define TENS_f_TRIS                 TRISCbits.TRISC5
#define TENS_f_LAT                  LATCbits.LATC5
#define TENS_f_PORT                 PORTCbits.RC5
#define TENS_f_ANS                  ANSELCbits.ANSC5
#define TENS_f_SetHigh()            do { LATCbits.LATC5 = 1; } while(0)
#define TENS_f_SetLow()             do { LATCbits.LATC5 = 0; } while(0)
#define TENS_f_Toggle()             do { LATCbits.LATC5 = ~LATCbits.LATC5; } while(0)
#define TENS_f_GetValue()           PORTCbits.RC5
#define TENS_f_SetDigitalInput()    do { TRISCbits.TRISC5 = 1; } while(0)
#define TENS_f_SetDigitalOutput()   do { TRISCbits.TRISC5 = 0; } while(0)
#define TENS_f_SetAnalogMode()      do { ANSELCbits.ANSC5 = 1; } while(0)
#define TENS_f_SetDigitalMode()     do { ANSELCbits.ANSC5 = 0; } while(0)

// get/set TENS_g aliases
#define TENS_g_TRIS                 TRISCbits.TRISC6
#define TENS_g_LAT                  LATCbits.LATC6
#define TENS_g_PORT                 PORTCbits.RC6
#define TENS_g_ANS                  ANSELCbits.ANSC6
#define TENS_g_SetHigh()            do { LATCbits.LATC6 = 1; } while(0)
#define TENS_g_SetLow()             do { LATCbits.LATC6 = 0; } while(0)
#define TENS_g_Toggle()             do { LATCbits.LATC6 = ~LATCbits.LATC6; } while(0)
#define TENS_g_GetValue()           PORTCbits.RC6
#define TENS_g_SetDigitalInput()    do { TRISCbits.TRISC6 = 1; } while(0)
#define TENS_g_SetDigitalOutput()   do { TRISCbits.TRISC6 = 0; } while(0)
#define TENS_g_SetAnalogMode()      do { ANSELCbits.ANSC6 = 1; } while(0)
#define TENS_g_SetDigitalMode()     do { ANSELCbits.ANSC6 = 0; } while(0)

// get/set TENS_dp aliases
#define TENS_dp_TRIS                 TRISCbits.TRISC7
#define TENS_dp_LAT                  LATCbits.LATC7
#define TENS_dp_PORT                 PORTCbits.RC7
#define TENS_dp_ANS                  ANSELCbits.ANSC7
#define TENS_dp_SetHigh()            do { LATCbits.LATC7 = 1; } while(0)
#define TENS_dp_SetLow()             do { LATCbits.LATC7 = 0; } while(0)
#define TENS_dp_Toggle()             do { LATCbits.LATC7 = ~LATCbits.LATC7; } while(0)
#define TENS_dp_GetValue()           PORTCbits.RC7
#define TENS_dp_SetDigitalInput()    do { TRISCbits.TRISC7 = 1; } while(0)
#define TENS_dp_SetDigitalOutput()   do { TRISCbits.TRISC7 = 0; } while(0)
#define TENS_dp_SetAnalogMode()      do { ANSELCbits.ANSC7 = 1; } while(0)
#define TENS_dp_SetDigitalMode()     do { ANSELCbits.ANSC7 = 0; } while(0)

// get/set ONES_a aliases
#define ONES_a_TRIS                 TRISDbits.TRISD0
#define ONES_a_LAT                  LATDbits.LATD0
#define ONES_a_PORT                 PORTDbits.RD0
#define ONES_a_ANS                  ANSELDbits.ANSD0
#define ONES_a_SetHigh()            do { LATDbits.LATD0 = 1; } while(0)
#define ONES_a_SetLow()             do { LATDbits.LATD0 = 0; } while(0)
#define ONES_a_Toggle()             do { LATDbits.LATD0 = ~LATDbits.LATD0; } while(0)
#define ONES_a_GetValue()           PORTDbits.RD0
#define ONES_a_SetDigitalInput()    do { TRISDbits.TRISD0 = 1; } while(0)
#define ONES_a_SetDigitalOutput()   do { TRISDbits.TRISD0 = 0; } while(0)
#define ONES_a_SetAnalogMode()      do { ANSELDbits.ANSD0 = 1; } while(0)
#define ONES_a_SetDigitalMode()     do { ANSELDbits.ANSD0 = 0; } while(0)

// get/set ONES_b aliases
#define ONES_b_TRIS                 TRISDbits.TRISD1
#define ONES_b_LAT                  LATDbits.LATD1
#define ONES_b_PORT                 PORTDbits.RD1
#define ONES_b_ANS                  ANSELDbits.ANSD1
#define ONES_b_SetHigh()            do { LATDbits.LATD1 = 1; } while(0)
#define ONES_b_SetLow()             do { LATDbits.LATD1 = 0; } while(0)
#define ONES_b_Toggle()             do { LATDbits.LATD1 = ~LATDbits.LATD1; } while(0)
#define ONES_b_GetValue()           PORTDbits.RD1
#define ONES_b_SetDigitalInput()    do { TRISDbits.TRISD1 = 1; } while(0)
#define ONES_b_SetDigitalOutput()   do { TRISDbits.TRISD1 = 0; } while(0)
#define ONES_b_SetAnalogMode()      do { ANSELDbits.ANSD1 = 1; } while(0)
#define ONES_b_SetDigitalMode()     do { ANSELDbits.ANSD1 = 0; } while(0)

// get/set ONES_c aliases
#define ONES_c_TRIS                 TRISDbits.TRISD2
#define ONES_c_LAT                  LATDbits.LATD2
#define ONES_c_PORT                 PORTDbits.RD2
#define ONES_c_ANS                  ANSELDbits.ANSD2
#define ONES_c_SetHigh()            do { LATDbits.LATD2 = 1; } while(0)
#define ONES_c_SetLow()             do { LATDbits.LATD2 = 0; } while(0)
#define ONES_c_Toggle()             do { LATDbits.LATD2 = ~LATDbits.LATD2; } while(0)
#define ONES_c_GetValue()           PORTDbits.RD2
#define ONES_c_SetDigitalInput()    do { TRISDbits.TRISD2 = 1; } while(0)
#define ONES_c_SetDigitalOutput()   do { TRISDbits.TRISD2 = 0; } while(0)
#define ONES_c_SetAnalogMode()      do { ANSELDbits.ANSD2 = 1; } while(0)
#define ONES_c_SetDigitalMode()     do { ANSELDbits.ANSD2 = 0; } while(0)

// get/set ONES_d aliases
#define ONES_d_TRIS                 TRISDbits.TRISD3
#define ONES_d_LAT                  LATDbits.LATD3
#define ONES_d_PORT                 PORTDbits.RD3
#define ONES_d_ANS                  ANSELDbits.ANSD3
#define ONES_d_SetHigh()            do { LATDbits.LATD3 = 1; } while(0)
#define ONES_d_SetLow()             do { LATDbits.LATD3 = 0; } while(0)
#define ONES_d_Toggle()             do { LATDbits.LATD3 = ~LATDbits.LATD3; } while(0)
#define ONES_d_GetValue()           PORTDbits.RD3
#define ONES_d_SetDigitalInput()    do { TRISDbits.TRISD3 = 1; } while(0)
#define ONES_d_SetDigitalOutput()   do { TRISDbits.TRISD3 = 0; } while(0)
#define ONES_d_SetAnalogMode()      do { ANSELDbits.ANSD3 = 1; } while(0)
#define ONES_d_SetDigitalMode()     do { ANSELDbits.ANSD3 = 0; } while(0)

// get/set ONES_e aliases
#define ONES_e_TRIS                 TRISDbits.TRISD4
#define ONES_e_LAT                  LATDbits.LATD4
#define ONES_e_PORT                 PORTDbits.RD4
#define ONES_e_ANS                  ANSELDbits.ANSD4
#define ONES_e_SetHigh()            do { LATDbits.LATD4 = 1; } while(0)
#define ONES_e_SetLow()             do { LATDbits.LATD4 = 0; } while(0)
#define ONES_e_Toggle()             do { LATDbits.LATD4 = ~LATDbits.LATD4; } while(0)
#define ONES_e_GetValue()           PORTDbits.RD4
#define ONES_e_SetDigitalInput()    do { TRISDbits.TRISD4 = 1; } while(0)
#define ONES_e_SetDigitalOutput()   do { TRISDbits.TRISD4 = 0; } while(0)
#define ONES_e_SetAnalogMode()      do { ANSELDbits.ANSD4 = 1; } while(0)
#define ONES_e_SetDigitalMode()     do { ANSELDbits.ANSD4 = 0; } while(0)

// get/set ONES_f aliases
#define ONES_f_TRIS                 TRISDbits.TRISD5
#define ONES_f_LAT                  LATDbits.LATD5
#define ONES_f_PORT                 PORTDbits.RD5
#define ONES_f_ANS                  ANSELDbits.ANSD5
#define ONES_f_SetHigh()            do { LATDbits.LATD5 = 1; } while(0)
#define ONES_f_SetLow()             do { LATDbits.LATD5 = 0; } while(0)
#define ONES_f_Toggle()             do { LATDbits.LATD5 = ~LATDbits.LATD5; } while(0)
#define ONES_f_GetValue()           PORTDbits.RD5
#define ONES_f_SetDigitalInput()    do { TRISDbits.TRISD5 = 1; } while(0)
#define ONES_f_SetDigitalOutput()   do { TRISDbits.TRISD5 = 0; } while(0)
#define ONES_f_SetAnalogMode()      do { ANSELDbits.ANSD5 = 1; } while(0)
#define ONES_f_SetDigitalMode()     do { ANSELDbits.ANSD5 = 0; } while(0)

// get/set ONES_g aliases
#define ONES_g_TRIS                 TRISDbits.TRISD6
#define ONES_g_LAT                  LATDbits.LATD6
#define ONES_g_PORT                 PORTDbits.RD6
#define ONES_g_ANS                  ANSELDbits.ANSD6
#define ONES_g_SetHigh()            do { LATDbits.LATD6 = 1; } while(0)
#define ONES_g_SetLow()             do { LATDbits.LATD6 = 0; } while(0)
#define ONES_g_Toggle()             do { LATDbits.LATD6 = ~LATDbits.LATD6; } while(0)
#define ONES_g_GetValue()           PORTDbits.RD6
#define ONES_g_SetDigitalInput()    do { TRISDbits.TRISD6 = 1; } while(0)
#define ONES_g_SetDigitalOutput()   do { TRISDbits.TRISD6 = 0; } while(0)
#define ONES_g_SetAnalogMode()      do { ANSELDbits.ANSD6 = 1; } while(0)
#define ONES_g_SetDigitalMode()     do { ANSELDbits.ANSD6 = 0; } while(0)

// get/set ONES_dp aliases
#define ONES_dp_TRIS                 TRISDbits.TRISD7
#define ONES_dp_LAT                  LATDbits.LATD7
#define ONES_dp_PORT                 PORTDbits.RD7
#define ONES_dp_ANS                  ANSELDbits.ANSD7
#define ONES_dp_SetHigh()            do { LATDbits.LATD7 = 1; } while(0)
#define ONES_dp_SetLow()             do { LATDbits.LATD7 = 0; } while(0)
#define ONES_dp_Toggle()             do { LATDbits.LATD7 = ~LATDbits.LATD7; } while(0)
#define ONES_dp_GetValue()           PORTDbits.RD7
#define ONES_dp_SetDigitalInput()    do { TRISDbits.TRISD7 = 1; } while(0)
#define ONES_dp_SetDigitalOutput()   do { TRISDbits.TRISD7 = 0; } while(0)
#define ONES_dp_SetAnalogMode()      do { ANSELDbits.ANSD7 = 1; } while(0)
#define ONES_dp_SetDigitalMode()     do { ANSELDbits.ANSD7 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/