/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.77
        Device            :  PIC18F45K22
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

//#include "mcc_generated_files/mcc.h"
#include "counter_funcs.h"

enum stateType {COUNT_UP_D, COUNT_DOWN_D, PAUSE, PAUSE_DB, COUNT_FIB, INIT_D, INIT_F, UD_DB};
static enum stateType state, prevState;

typedef union inputs {
    struct {
        uint8_t ON_SW:      1; //LSB
        uint8_t OFF_SW:     1;
        uint8_t UD_SW:      1;
        uint8_t TOG_SW:     1;
        uint8_t RST_SW:     1;
        uint8_t unused :    3;
    }inny;
    uint8_t value;
}StatusReg;
/*
                         Main application
 */
void main(void)
{
    // Initialize the device
    static int fibCount;
    static int decCount;
    static int l1 = 0;
    static int l2 = 1;
    int tens, ones;
   
    SYSTEM_Initialize();
    
    testDigits();
    
    state = INIT_D;
    
    StatusReg inputs;

    while (1)
    {
        
        inputs.inny.ON_SW = ON_SW_GetValue();
        inputs.inny.OFF_SW = OFF_SW_GetValue();
        inputs.inny.UD_SW = UD_SW_GetValue();
        inputs.inny.TOG_SW = TOG_SW_GetValue();
        inputs.inny.RST_SW = RST_SW_GetValue();

        switch(state){
            case INIT_D:
                decCount = 0;
                displayDigits(0,0);
                if(inputs.inny.ON_SW == 1){
                    state = COUNT_UP_D;
                }
                break;
            case COUNT_UP_D: 
                displayDigits(extractTENS(decCount), extractONES(decCount));
                if(decCount == 99){
                    decCount = 0;
                    __delay_ms(500);
                }else if(inputs.inny.RST_SW == 1){
                    state = INIT_D;
                }else if(inputs.inny.OFF_SW == 1){
                    prevState = state;
                    state = PAUSE;
                    __delay_ms(500);
                }else if(inputs.inny.UD_SW == 1){
                    prevState = state;
                    state = UD_DB;
                }else if(inputs.inny.TOG_SW == 1){
                    state = INIT_F;
                }else{
                    decCount += 1; 
                    __delay_ms(500);
                }
                break;
            case UD_DB: //updown switch debounce state
                __delay_ms(150);
                if(inputs.inny.UD_SW == 0){
                    if(prevState == COUNT_UP_D){
                            state = COUNT_DOWN_D;
                        }else if(prevState == COUNT_DOWN_D){
                            state = COUNT_UP_D;
                        }
                }
                break;
            case COUNT_DOWN_D:
                displayDigits(extractTENS(decCount), extractONES(decCount));
                if(decCount == 0){
                    decCount = 99;
                    __delay_ms(500);
                }else if(inputs.inny.RST_SW == 1){
                    state = INIT_D;
                }else if(inputs.inny.OFF_SW == 1){
                    prevState = state;
                    state = PAUSE;
                }else if(inputs.inny.UD_SW == 1){
                    prevState = state;
                    state = UD_DB;   
                }else if(inputs.inny.TOG_SW == 1){
                    state = INIT_F;
                }else{
                    decCount -= 1;
                    __delay_ms(500);
                }
                break;
            case PAUSE:
                displayDigits(extractTENS(decCount), extractONES(decCount));
                if(inputs.inny.ON_SW == 1){
                    state = PAUSE_DB;
                }
                break;
            case PAUSE_DB:
                __delay_ms(150);
                if(inputs.inny.ON_SW == 0){
                    if(prevState == COUNT_UP_D){
                            state = COUNT_UP_D;
                    }else if(prevState == COUNT_DOWN_D){
                        state = COUNT_DOWN_D;
                    }else if(prevState == COUNT_FIB){
                        state == COUNT_FIB;
                    }
                }
                break;
            case COUNT_FIB: //fibonacci sequence only counts up
                displayDigits(extractTENS(fibCount), extractONES(fibCount));
                if(fibCount == 89){
                    fibCount = 0;
                    l1 = 0;
                    l2 = 1;
                    __delay_ms(500);
                }else if(inputs.inny.RST_SW == 1){
                    state = INIT_F;
                }else if(inputs.inny.OFF_SW == 1){
                    prevState = state;
                    state = PAUSE;   
                }else if(inputs.inny.TOG_SW == 1){
                    state = INIT_D;
                }else{
                    fibCount = l1 + l2;
                    l1 = l2;
                    l2 = fibCount;
                    __delay_ms(500);
                }
                break;
            case INIT_F:
                displayDigits(0,0);
                if(inputs.inny.ON_SW == 1){
                    __delay_ms(500);
                    state = COUNT_FIB;
                }
                break;
        }
        
    }
}
/**
 End of File
 * */