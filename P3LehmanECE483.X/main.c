/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.77
        Device            :  PIC18F45K22
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "mcc_generated_files/mcc.h"

enum MAJOR_STATE {ON, OFF, DISPATCH, ALARM, FINAL_P, LISTEN}; //added dispatcher state
enum ALARM_STATE {ESTOP, OILP};
static enum MAJOR_STATE state, prevState;
static enum ALARM_STATE aState;

typedef union inputs {
    struct {
        uint8_t finalP:         1;
        uint8_t oilP:           1;
        uint8_t reset:          1;
        uint8_t e_STOP:         1;
        uint8_t OFF:            1;
        uint8_t ON :            1;
        uint8_t AlarmFdbck :    1;
        uint8_t RunFdbck :      1;
        uint8_t unused :        8;
    }inny;
    uint16_t value;
}StatusReg;

/*
                         Main application
 */
void main(void)
{
    // Initialize the device
    SYSTEM_Initialize();

    StatusReg InReg;
    POWER_ON_SetHigh();
    
    state = DISPATCH;

    while (1)
    {        
          //state = inputs2State();  
        
        InReg.inny.finalP = FINAL_P_GetValue();  
        InReg.inny.oilP = OIL_P_GetValue();
        InReg.inny.reset = RESET_GetValue(); 
        InReg.inny.e_STOP = ESTOP_GetValue(); 
        InReg.inny.OFF = OFF_SW_GetValue(); 
        InReg.inny.ON = ON_SW_GetValue();
        InReg.inny.AlarmFdbck = UNIT2_ALARMsig_GetValue();
        InReg.inny.RunFdbck = UNIT2_RUN_GetValue();

            switch(state){
                case ON:
                    UNIT_RUN_OUT_SetHigh();
                    COMP_ON_SetHigh(); //turn on
                    ALARM_SetLow();
                    if(InReg.inny.finalP == 0){
                        state = FINAL_P;                    
                    }else if(InReg.inny.OFF == 1){
                        state = DISPATCH;
                    }else if(InReg.inny.e_STOP == 0){
                        state = ALARM;
                        aState = ESTOP;
                    }else if(InReg.inny.oilP == 0){
                        state = ALARM;
                        aState = OILP;
                    }else{
                        MOTOR1_SetHigh(); //chasing LEDs for motor
                        __delay_ms(50);
                        MOTOR2_SetHigh();
                        MOTOR1_SetLow();
                        __delay_ms(50);
                        MOTOR3_SetHigh();
                        MOTOR2_SetLow();
                        __delay_ms(50);
                        MOTOR3_SetLow();
                        }
                    break;
                case FINAL_P:
                    UNIT_RUN_OUT_SetLow();
                    COMP_ON_SetLow();
                    MOTOR1_SetLow();
                    MOTOR2_SetLow();
                    MOTOR3_SetLow();
                    if(InReg.inny.RunFdbck == 0 && InReg.inny.finalP == 1){
                        state = ON;
                    }
                    break;
                case DISPATCH:
                    UNIT_RUN_OUT_SetLow();
                    COMP_ON_SetLow();
                    ALARM_SetLow();
                    if(InReg.inny.e_STOP == 0){
                        state = ALARM;
                        aState = ESTOP;
                    }else if(InReg.inny.ON == 1 && InReg.inny.RunFdbck == 0){
                        state = ON;
                    }else if(InReg.inny.RunFdbck == 1){
                        state = LISTEN;
                    }else if(InReg.inny.AlarmFdbck == 1){
                        state = ON;
                    }else if(InReg.inny.OFF == 1){
                        state = OFF;
                    }else{
                        MOTOR1_SetLow();
                        MOTOR2_SetLow();
                        MOTOR3_SetLow();
                    }
                    break;
                case OFF:
                    UNIT_RUN_OUT_SetLow();
                    ALARM_SetLow();
                    if(InReg.inny.ON == 1 && InReg.inny.RunFdbck == 0){
                        state = ON;
                    }else if(InReg.inny.AlarmFdbck == 1){
                        state = ON;
                    }else if(InReg.inny.e_STOP == 0){
                        state = ALARM;
                        aState = ESTOP;
                    }else{
                        MOTOR1_SetLow();
                        MOTOR2_SetLow();
                        MOTOR3_SetLow();
                    }
                    break;
                case ALARM:
                    COMP_ON_SetLow();
                    UNIT_RUN_OUT_SetLow();
                    if(InReg.inny.reset == 1){
                        state = DISPATCH;
                    }else{
                        switch(aState){
                            case ESTOP:
                                ALARM_Toggle();
                                __delay_ms(500);
                                break;
                            case OILP:
                                ALARM_Toggle();
                                __delay_ms(250);
                                break;
                        }                      
                    }
                    break;
                case LISTEN:
                    if(InReg.inny.RunFdbck == 0){
                        state = ON;
                    }
                    break;
            }
          
    }
}

/**
 End of File
*/