/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.77
        Device            :  PIC18F45K22
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.05 and above
        MPLAB 	          :  MPLAB X 5.20	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set ESTOP aliases
#define ESTOP_TRIS                 TRISAbits.TRISA0
#define ESTOP_LAT                  LATAbits.LATA0
#define ESTOP_PORT                 PORTAbits.RA0
#define ESTOP_ANS                  ANSELAbits.ANSA0
#define ESTOP_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define ESTOP_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define ESTOP_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define ESTOP_GetValue()           PORTAbits.RA0
#define ESTOP_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define ESTOP_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define ESTOP_SetAnalogMode()      do { ANSELAbits.ANSA0 = 1; } while(0)
#define ESTOP_SetDigitalMode()     do { ANSELAbits.ANSA0 = 0; } while(0)

// get/set RESET aliases
#define RESET_TRIS                 TRISAbits.TRISA1
#define RESET_LAT                  LATAbits.LATA1
#define RESET_PORT                 PORTAbits.RA1
#define RESET_ANS                  ANSELAbits.ANSA1
#define RESET_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define RESET_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define RESET_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define RESET_GetValue()           PORTAbits.RA1
#define RESET_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define RESET_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define RESET_SetAnalogMode()      do { ANSELAbits.ANSA1 = 1; } while(0)
#define RESET_SetDigitalMode()     do { ANSELAbits.ANSA1 = 0; } while(0)

// get/set OIL_P aliases
#define OIL_P_TRIS                 TRISAbits.TRISA2
#define OIL_P_LAT                  LATAbits.LATA2
#define OIL_P_PORT                 PORTAbits.RA2
#define OIL_P_ANS                  ANSELAbits.ANSA2
#define OIL_P_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define OIL_P_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define OIL_P_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define OIL_P_GetValue()           PORTAbits.RA2
#define OIL_P_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define OIL_P_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define OIL_P_SetAnalogMode()      do { ANSELAbits.ANSA2 = 1; } while(0)
#define OIL_P_SetDigitalMode()     do { ANSELAbits.ANSA2 = 0; } while(0)

// get/set FINAL_P aliases
#define FINAL_P_TRIS                 TRISAbits.TRISA3
#define FINAL_P_LAT                  LATAbits.LATA3
#define FINAL_P_PORT                 PORTAbits.RA3
#define FINAL_P_ANS                  ANSELAbits.ANSA3
#define FINAL_P_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define FINAL_P_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define FINAL_P_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define FINAL_P_GetValue()           PORTAbits.RA3
#define FINAL_P_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define FINAL_P_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define FINAL_P_SetAnalogMode()      do { ANSELAbits.ANSA3 = 1; } while(0)
#define FINAL_P_SetDigitalMode()     do { ANSELAbits.ANSA3 = 0; } while(0)

// get/set OFF_SW aliases
#define OFF_SW_TRIS                 TRISAbits.TRISA5
#define OFF_SW_LAT                  LATAbits.LATA5
#define OFF_SW_PORT                 PORTAbits.RA5
#define OFF_SW_ANS                  ANSELAbits.ANSA5
#define OFF_SW_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define OFF_SW_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define OFF_SW_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define OFF_SW_GetValue()           PORTAbits.RA5
#define OFF_SW_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define OFF_SW_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define OFF_SW_SetAnalogMode()      do { ANSELAbits.ANSA5 = 1; } while(0)
#define OFF_SW_SetDigitalMode()     do { ANSELAbits.ANSA5 = 0; } while(0)

// get/set ON_SW aliases
#define ON_SW_TRIS                 TRISBbits.TRISB0
#define ON_SW_LAT                  LATBbits.LATB0
#define ON_SW_PORT                 PORTBbits.RB0
#define ON_SW_WPU                  WPUBbits.WPUB0
#define ON_SW_ANS                  ANSELBbits.ANSB0
#define ON_SW_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define ON_SW_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define ON_SW_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define ON_SW_GetValue()           PORTBbits.RB0
#define ON_SW_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define ON_SW_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define ON_SW_SetPullup()          do { WPUBbits.WPUB0 = 1; } while(0)
#define ON_SW_ResetPullup()        do { WPUBbits.WPUB0 = 0; } while(0)
#define ON_SW_SetAnalogMode()      do { ANSELBbits.ANSB0 = 1; } while(0)
#define ON_SW_SetDigitalMode()     do { ANSELBbits.ANSB0 = 0; } while(0)

// get/set MOTOR1 aliases
#define MOTOR1_TRIS                 TRISBbits.TRISB1
#define MOTOR1_LAT                  LATBbits.LATB1
#define MOTOR1_PORT                 PORTBbits.RB1
#define MOTOR1_WPU                  WPUBbits.WPUB1
#define MOTOR1_ANS                  ANSELBbits.ANSB1
#define MOTOR1_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define MOTOR1_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define MOTOR1_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define MOTOR1_GetValue()           PORTBbits.RB1
#define MOTOR1_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define MOTOR1_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define MOTOR1_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define MOTOR1_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define MOTOR1_SetAnalogMode()      do { ANSELBbits.ANSB1 = 1; } while(0)
#define MOTOR1_SetDigitalMode()     do { ANSELBbits.ANSB1 = 0; } while(0)

// get/set MOTOR2 aliases
#define MOTOR2_TRIS                 TRISBbits.TRISB2
#define MOTOR2_LAT                  LATBbits.LATB2
#define MOTOR2_PORT                 PORTBbits.RB2
#define MOTOR2_WPU                  WPUBbits.WPUB2
#define MOTOR2_ANS                  ANSELBbits.ANSB2
#define MOTOR2_SetHigh()            do { LATBbits.LATB2 = 1; } while(0)
#define MOTOR2_SetLow()             do { LATBbits.LATB2 = 0; } while(0)
#define MOTOR2_Toggle()             do { LATBbits.LATB2 = ~LATBbits.LATB2; } while(0)
#define MOTOR2_GetValue()           PORTBbits.RB2
#define MOTOR2_SetDigitalInput()    do { TRISBbits.TRISB2 = 1; } while(0)
#define MOTOR2_SetDigitalOutput()   do { TRISBbits.TRISB2 = 0; } while(0)
#define MOTOR2_SetPullup()          do { WPUBbits.WPUB2 = 1; } while(0)
#define MOTOR2_ResetPullup()        do { WPUBbits.WPUB2 = 0; } while(0)
#define MOTOR2_SetAnalogMode()      do { ANSELBbits.ANSB2 = 1; } while(0)
#define MOTOR2_SetDigitalMode()     do { ANSELBbits.ANSB2 = 0; } while(0)

// get/set MOTOR3 aliases
#define MOTOR3_TRIS                 TRISBbits.TRISB3
#define MOTOR3_LAT                  LATBbits.LATB3
#define MOTOR3_PORT                 PORTBbits.RB3
#define MOTOR3_WPU                  WPUBbits.WPUB3
#define MOTOR3_ANS                  ANSELBbits.ANSB3
#define MOTOR3_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define MOTOR3_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define MOTOR3_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define MOTOR3_GetValue()           PORTBbits.RB3
#define MOTOR3_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define MOTOR3_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define MOTOR3_SetPullup()          do { WPUBbits.WPUB3 = 1; } while(0)
#define MOTOR3_ResetPullup()        do { WPUBbits.WPUB3 = 0; } while(0)
#define MOTOR3_SetAnalogMode()      do { ANSELBbits.ANSB3 = 1; } while(0)
#define MOTOR3_SetDigitalMode()     do { ANSELBbits.ANSB3 = 0; } while(0)

// get/set UNIT2_RUN aliases
#define UNIT2_RUN_TRIS                 TRISCbits.TRISC0
#define UNIT2_RUN_LAT                  LATCbits.LATC0
#define UNIT2_RUN_PORT                 PORTCbits.RC0
#define UNIT2_RUN_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define UNIT2_RUN_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define UNIT2_RUN_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define UNIT2_RUN_GetValue()           PORTCbits.RC0
#define UNIT2_RUN_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define UNIT2_RUN_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)

// get/set UNIT2_ALARMsig aliases
#define UNIT2_ALARMsig_TRIS                 TRISCbits.TRISC1
#define UNIT2_ALARMsig_LAT                  LATCbits.LATC1
#define UNIT2_ALARMsig_PORT                 PORTCbits.RC1
#define UNIT2_ALARMsig_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define UNIT2_ALARMsig_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define UNIT2_ALARMsig_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define UNIT2_ALARMsig_GetValue()           PORTCbits.RC1
#define UNIT2_ALARMsig_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define UNIT2_ALARMsig_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)

// get/set UNIT_RUN_OUT aliases
#define UNIT_RUN_OUT_TRIS                 TRISCbits.TRISC2
#define UNIT_RUN_OUT_LAT                  LATCbits.LATC2
#define UNIT_RUN_OUT_PORT                 PORTCbits.RC2
#define UNIT_RUN_OUT_ANS                  ANSELCbits.ANSC2
#define UNIT_RUN_OUT_SetHigh()            do { LATCbits.LATC2 = 1; } while(0)
#define UNIT_RUN_OUT_SetLow()             do { LATCbits.LATC2 = 0; } while(0)
#define UNIT_RUN_OUT_Toggle()             do { LATCbits.LATC2 = ~LATCbits.LATC2; } while(0)
#define UNIT_RUN_OUT_GetValue()           PORTCbits.RC2
#define UNIT_RUN_OUT_SetDigitalInput()    do { TRISCbits.TRISC2 = 1; } while(0)
#define UNIT_RUN_OUT_SetDigitalOutput()   do { TRISCbits.TRISC2 = 0; } while(0)
#define UNIT_RUN_OUT_SetAnalogMode()      do { ANSELCbits.ANSC2 = 1; } while(0)
#define UNIT_RUN_OUT_SetDigitalMode()     do { ANSELCbits.ANSC2 = 0; } while(0)

// get/set POWER_ON aliases
#define POWER_ON_TRIS                 TRISDbits.TRISD0
#define POWER_ON_LAT                  LATDbits.LATD0
#define POWER_ON_PORT                 PORTDbits.RD0
#define POWER_ON_ANS                  ANSELDbits.ANSD0
#define POWER_ON_SetHigh()            do { LATDbits.LATD0 = 1; } while(0)
#define POWER_ON_SetLow()             do { LATDbits.LATD0 = 0; } while(0)
#define POWER_ON_Toggle()             do { LATDbits.LATD0 = ~LATDbits.LATD0; } while(0)
#define POWER_ON_GetValue()           PORTDbits.RD0
#define POWER_ON_SetDigitalInput()    do { TRISDbits.TRISD0 = 1; } while(0)
#define POWER_ON_SetDigitalOutput()   do { TRISDbits.TRISD0 = 0; } while(0)
#define POWER_ON_SetAnalogMode()      do { ANSELDbits.ANSD0 = 1; } while(0)
#define POWER_ON_SetDigitalMode()     do { ANSELDbits.ANSD0 = 0; } while(0)

// get/set COMP_ON aliases
#define COMP_ON_TRIS                 TRISDbits.TRISD1
#define COMP_ON_LAT                  LATDbits.LATD1
#define COMP_ON_PORT                 PORTDbits.RD1
#define COMP_ON_ANS                  ANSELDbits.ANSD1
#define COMP_ON_SetHigh()            do { LATDbits.LATD1 = 1; } while(0)
#define COMP_ON_SetLow()             do { LATDbits.LATD1 = 0; } while(0)
#define COMP_ON_Toggle()             do { LATDbits.LATD1 = ~LATDbits.LATD1; } while(0)
#define COMP_ON_GetValue()           PORTDbits.RD1
#define COMP_ON_SetDigitalInput()    do { TRISDbits.TRISD1 = 1; } while(0)
#define COMP_ON_SetDigitalOutput()   do { TRISDbits.TRISD1 = 0; } while(0)
#define COMP_ON_SetAnalogMode()      do { ANSELDbits.ANSD1 = 1; } while(0)
#define COMP_ON_SetDigitalMode()     do { ANSELDbits.ANSD1 = 0; } while(0)

// get/set ALARM aliases
#define ALARM_TRIS                 TRISDbits.TRISD2
#define ALARM_LAT                  LATDbits.LATD2
#define ALARM_PORT                 PORTDbits.RD2
#define ALARM_ANS                  ANSELDbits.ANSD2
#define ALARM_SetHigh()            do { LATDbits.LATD2 = 1; } while(0)
#define ALARM_SetLow()             do { LATDbits.LATD2 = 0; } while(0)
#define ALARM_Toggle()             do { LATDbits.LATD2 = ~LATDbits.LATD2; } while(0)
#define ALARM_GetValue()           PORTDbits.RD2
#define ALARM_SetDigitalInput()    do { TRISDbits.TRISD2 = 1; } while(0)
#define ALARM_SetDigitalOutput()   do { TRISDbits.TRISD2 = 0; } while(0)
#define ALARM_SetAnalogMode()      do { ANSELDbits.ANSD2 = 1; } while(0)
#define ALARM_SetDigitalMode()     do { ANSELDbits.ANSD2 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/